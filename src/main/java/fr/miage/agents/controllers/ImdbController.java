package fr.miage.agents.controllers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;

import org.json.JSONArray;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.miage.agents.models.Episode;
import fr.miage.agents.models.FilmSerie;
import fr.miage.agents.models.Genre;
import fr.miage.agents.models.Personne;
import fr.miage.agents.repositories.EpisodeRepository;
import fr.miage.agents.repositories.FilmSerieRepository;
import fr.miage.agents.repositories.GenreRepository;
import fr.miage.agents.repositories.PersonneRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ImdbController {

    public static final Long MAX_TITLE_ID = 1000L;
    public static final int IMPORT_DELAY = 0;

    private final FilmSerieRepository filmSerieRepository;
    private final EpisodeRepository episodeRepository;
    private final GenreRepository genreRepository;
    private final PersonneRepository personneRepository;

    private TsvParserSettings settings = null;

    public void initializeParserSettings() {
        if (settings == null) {
            settings = new TsvParserSettings();
            settings.getFormat().setLineSeparator("\n");
            settings.setHeaderExtractionEnabled(true);
            settings.setMaxCharsPerColumn(100000);
            settings.setNullValue("\\N");
        }
    }

    public void importAll() throws FileNotFoundException, InterruptedException {
        initializeParserSettings();

        importTitleBasics();
        importNameBasics();
        importTitleCrew();
        importEpisodes();
        importPrincipals();
    }

    public void importTitleBasics() throws FileNotFoundException, InterruptedException {
        System.out.println("Import title basics");
        initializeParserSettings();
        TsvParser parser = new TsvParser(settings);
        FileReader reader = new FileReader("data/imdb/title_basics.tsv");
        for (String[] title : parser.iterate(reader)) {
            try {
                Long titleID = Long.parseLong(getMatch("(?:\\d+)", title[0]), 10);
                if (titleID > MAX_TITLE_ID) {
                    System.out.println("reached max title, giving up");
                    break;
                }
                importTitle(title);

                System.out.println("title " + title[0] + " imported");
            } catch (Exception e) {
                System.out.println("title " + title[0] + " import failed, skipped");
                Thread.sleep(100);
            }
            Thread.sleep(IMPORT_DELAY);
        }
        parser.stopParsing();
    }

    public void importNameBasics() throws FileNotFoundException, InterruptedException {
        System.out.println("Import name basics");
        initializeParserSettings();
        TsvParser parser = new TsvParser(settings);
        FileReader reader = new FileReader("data/imdb/name_basics.tsv");
        for (String[] name : parser.iterate(reader)) {
            try {
                importName(name);
                System.out.println("name " + name[0] + " imported");
            } catch (Exception e) {
                System.out.println("name " + name[0] + " import failed, skipped");
                Thread.sleep(100);
            }
            Thread.sleep(IMPORT_DELAY);
        }
        parser.stopParsing();
    }

    public void importTitleCrew() throws FileNotFoundException, InterruptedException {
        System.out.println("Import crew");
        initializeParserSettings();

        TsvParser parser = new TsvParser(settings);
        FileReader reader = new FileReader("data/imdb/crew.tsv");
        for (String[] crew : parser.iterate(reader)) {
            try {
                Long titleID = Long.parseLong(getMatch("(?:\\d+)", crew[0]), 10);
                if (titleID > MAX_TITLE_ID) {
                    System.out.println("reached max title, ignored");
                    break;
                }
                updateTitleCrew(crew);
                System.out.println("crew for title " + crew[0] + " imported.");

            } catch (Exception e) {
                System.out.println("crew for title " + crew[0] + " import failed, skipped");
                Thread.sleep(100);
            }
            Thread.sleep(IMPORT_DELAY);
        }
        parser.stopParsing();
    }

    public void importEpisodes() throws FileNotFoundException, InterruptedException {
        System.out.println("Import episodes");
        initializeParserSettings();

        TsvParser parser = new TsvParser(settings);
        FileReader reader = new FileReader("data/imdb/episode.tsv");
        for (String[] episode : parser.iterate(reader)) {
            try {
                Long titleID = Long.parseLong(getMatch("(?:\\d+)", episode[0]), 10);
                if (titleID > MAX_TITLE_ID) {
                    System.out.println("reached max title, ignored");
                    break;
                }
                updateEpisode(episode);
                System.out.println("episode " + episode[0] + " updated.");

            } catch (Exception e) {
                System.out.println("episode " + episode[0] + " update failed, skipped");
                Thread.sleep(100);
            }
            Thread.sleep(IMPORT_DELAY);
        }
        parser.stopParsing();
    }

    public void importPrincipals() throws FileNotFoundException, InterruptedException {
        System.out.println("Import principals");
        initializeParserSettings();

        TsvParser parser = new TsvParser(settings);
        FileReader reader = new FileReader("data/imdb/principals.tsv");
        for (String[] principal : parser.iterate(reader)) {
            try {
                Long titleID = Long.parseLong(getMatch("(?:\\d+)", principal[0]), 10);
                if (titleID > MAX_TITLE_ID) {
                    System.out.println("reached max title, ignored");
                    break;
                }
                importPrincipal(principal);

                System.out.println("principal for title " + principal[0] + " imported.");

            } catch (Exception e) {
                System.out.println("principal for title " + principal[0] + " import failed, skipped");
                Thread.sleep(100);
            }
            Thread.sleep(IMPORT_DELAY);
        }
        parser.stopParsing();
    }

    @Transactional
    private void importTitle(String[] imdbTitle) throws Exception {
        System.out.println("IMDB Title: " + Arrays.toString(imdbTitle));

        String titre = imdbTitle[2];

        // date
        Date dateSortie = null;
        String anneeSortie = getMatch("(?:\\d+)", imdbTitle[5]);
        if (anneeSortie != null) {
            dateSortie = new Date(Integer.parseInt(anneeSortie));
        }

        Duration duree = null;
        String dureeString = getMatch("(?:\\d+)", imdbTitle[7]);
        if (dureeString != null) {
            duree = Duration.ofMinutes(Integer.parseInt(dureeString));
        }

        List<Genre> genres = new ArrayList<Genre>();
        if (imdbTitle[8] != "\\N") {
            for (String genreName : imdbTitle[8].split(",")) {
                Genre genre;
                Optional<Genre> optionalGenre = genreRepository.findByName(genreName);
                if (!optionalGenre.isPresent()) {
                    genre = new Genre(genreName);
                    genreRepository.save(genre);
                } else {
                    genre = optionalGenre.get();
                }

                genres.add(genre);
            }
        }

        if (imdbTitle[1] == "tvepisode") {
            Episode episode = new Episode(imdbTitle[0], titre, null, dateSortie, genres, duree);
            episodeRepository.save(episode);
        } else {
            FilmSerie filmSerie = new FilmSerie(imdbTitle[0], titre, null, dateSortie, genres, duree);
            filmSerieRepository.save(filmSerie);
        }
    }

    @Transactional
    private void importName(String[] imdbName) throws Exception {
        System.out.println("IMDB Name: " + Arrays.toString(imdbName));

        String[] names = imdbName[1].split(" ");
        String prenom = names[0];
        String nom = null;
        if (names.length > 1) {
            nom = names[1];
        }

        Personne personne = new Personne(imdbName[0], prenom, nom);

        personneRepository.save(personne);
    }

    @Transactional
    private void updateTitleCrew(String[] imdbCrew) throws Exception {
        System.out.println("IMDB Crew: " + Arrays.toString(imdbCrew));

        Optional<FilmSerie> optionalFilmSerie = filmSerieRepository.findByExternalID(imdbCrew[0]);
        if (!optionalFilmSerie.isPresent()) {
            throw new NotFoundException();
        }

        FilmSerie filmSerie = optionalFilmSerie.get();

        // add realisateurs
        if (!isNull(imdbCrew[1])) {
            for (String director : imdbCrew[1].split(",")) {
                // get personne
                Optional<Personne> optionalPersonne = personneRepository.findByExternalID(director);
                if (optionalPersonne.isPresent()) {
                    filmSerie.getRealisateurs().add(optionalPersonne.get());
                } else {
                    System.out.println("Peronne " + director + " does not exists, skipped.");
                }
            }
        }

        // add scenaristes
        if (!isNull(imdbCrew[2])) {
            for (String writer : imdbCrew[2].split(",")) {
                // get personne
                Optional<Personne> optionalPersonne = personneRepository.findByExternalID(writer);
                if (optionalPersonne.isPresent()) {
                    filmSerie.getScenaristes().add(optionalPersonne.get());
                } else {
                    System.out.println("Peronne " + writer + " does not exists, skipped.");
                }
            }
        }

        filmSerieRepository.save(filmSerie);
    }

    @Transactional
    private void updateEpisode(String[] imdbEpisode) throws Exception {
        System.out.println("IMDB episode: " + Arrays.toString(imdbEpisode));

        Optional<Episode> optionalEpisode = episodeRepository.findByExternalID(imdbEpisode[0]);
        if (!optionalEpisode.isPresent()) {
            throw new NotFoundException();
        }

        Episode episode = optionalEpisode.get();

        if (!isNull(imdbEpisode[2])) {
            episode.setSaison(Integer.parseInt(imdbEpisode[2]));
        }

        if (!isNull(imdbEpisode[3])) {
            episode.setEpisode(Integer.parseInt(imdbEpisode[3]));
        }

        if (!isNull(imdbEpisode[4])) {
            Long filmSerieID = Long.parseLong(getMatch("(?:\\d+)", imdbEpisode[5]));
            Optional<FilmSerie> optionalFilmSerie = filmSerieRepository.findById(filmSerieID);
            if (!optionalFilmSerie.isPresent()) {
                throw new NotFoundException();
            }
            episode.setSerie(optionalFilmSerie.get());
        }

        episodeRepository.save(episode);
    }

    @Transactional
    private void importPrincipal(String[] imdbPrincipal) throws Exception {
        System.out.println("IMDB Principal: " + Arrays.toString(imdbPrincipal));

        Optional<FilmSerie> optionalFilmSerie = filmSerieRepository.findByExternalID(imdbPrincipal[0]);
        System.out.println(optionalFilmSerie);
        if (!optionalFilmSerie.isPresent()) {
            throw new NotFoundException();
        }

        FilmSerie filmSerie = optionalFilmSerie.get();

        Optional<Personne> optionalPersonne = personneRepository.findByExternalID(imdbPrincipal[2]);
        if (!optionalPersonne.isPresent()) {
            throw new NotFoundException();
        }

        filmSerie.getActeurs().add(optionalPersonne.get());
        if (!isNull(imdbPrincipal[5])) {

            Object[] characters = new JSONArray(imdbPrincipal[5]).toList().toArray();
            for (Object characterObj : characters) {
                String character = (String) characterObj;
                if (!filmSerie.getKeywords().contains(character)){
                    filmSerie.getKeywords().add(character);
                }
            }
        }

        filmSerieRepository.save(filmSerie);
    }

    private String getMatch(String regexString, String source) {
        Pattern regex = Pattern.compile(regexString);
        Matcher matcher = regex.matcher(source);
        if (!matcher.find()) {
            return null;
        }
        return matcher.group(0);
    }

    private boolean isNull(String source) {
        return source.equals("\\N");
    }
}
