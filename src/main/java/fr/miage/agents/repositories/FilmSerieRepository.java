package fr.miage.agents.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.miage.agents.models.FilmSerie;

@Repository
public interface FilmSerieRepository extends JpaRepository<FilmSerie, Long> {
    Optional<FilmSerie> findByExternalID(String externalID);
}
