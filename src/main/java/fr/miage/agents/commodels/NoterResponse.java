package fr.miage.agents.commodels;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NoterResponse {
    private Long noteurID;
    private String typeSujet;
    private Long sujetID;
    private int note;
}
