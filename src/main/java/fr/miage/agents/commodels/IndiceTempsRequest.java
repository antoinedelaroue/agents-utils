package fr.miage.agents.commodels;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IndiceTempsRequest extends IndiceRequest{
    private Date debut;
    private Date fin;
}
