package fr.miage.agents.commodels;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IndiceResponse {
    private int indice;
    private Date date;
}
