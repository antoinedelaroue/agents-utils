package fr.miage.agents.commodels;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IndiceRequest {
    private String typeSujet;
    private int sujetID;
}
