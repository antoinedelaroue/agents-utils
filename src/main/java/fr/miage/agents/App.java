package fr.miage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fr.miage.agents.controllers.ImdbController;

@SpringBootApplication
public class App implements CommandLineRunner {

  @Autowired
  private ImdbController imdbController;

  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    System.out.println("Agents database feeder");

    if (args.length > 0) {
      switch (args[0]) {
        case "imdb":
          switch (args[1]) {
            case "all":
              imdbController.importAll();
              break;
            case "titles":
              imdbController.importTitleBasics();
              break;
            case "names":
              imdbController.importNameBasics();
              break;
            case "crew":
              imdbController.importTitleCrew();
              break;
            case "principals":
              imdbController.importPrincipals();
              break;
            case "episodes":
              imdbController.importEpisodes();
              break;
            default:
              System.out.println("please say what do you want to import ? (all, titles, names, crew or principals)");
          }
          break;
        default:
          System.out.println("command not found");
      }
      System.out.println("Terminé.");
    } else {
      System.out.println("please provide command");
    }
  }
}
