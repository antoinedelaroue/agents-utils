package fr.miage.agents.models;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Jeu extends Oeuvre {

    @OneToOne
	private Organisation developpeur;
}
