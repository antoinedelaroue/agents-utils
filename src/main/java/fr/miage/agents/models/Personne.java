package fr.miage.agents.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(indexes = { @Index(name = "IDX_EXTERNALID", columnList = "externalID") })
public class Personne {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String externalID;
    private String nom;    
    private String prenom;

    public Personne(String externalID, String prenom, String nom){
        this.externalID = externalID;
        this.prenom = prenom;
        this.nom = nom;
    }
}
