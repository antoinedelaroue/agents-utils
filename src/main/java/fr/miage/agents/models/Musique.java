package fr.miage.agents.models;

import java.time.Duration;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Musique extends Oeuvre {

    private Duration duree;

    @ManyToOne
	private Album album;
}
