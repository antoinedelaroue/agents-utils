package fr.miage.agents.models;

import java.time.Duration;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Episode extends Oeuvre {

    private Duration duree;

    private int saison;

    private int episode;

    @OneToOne
	private FilmSerie serie;

    public Episode(String externalID, String titre, String description, Date dateSortie, List<Genre> genres, Duration duree){
        super(externalID, titre, description, dateSortie, genres);
        this.duree = duree;
    }
}
