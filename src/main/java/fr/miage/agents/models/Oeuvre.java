package fr.miage.agents.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Oeuvre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String externalID;
    private String titre;
    private String description;
    private Date dateSortie;

    @ElementCollection
    private List<String> keywords = new ArrayList<String>();

    @ManyToMany
    private List<Genre> genres;

    public Oeuvre(String externalID, String titre, String description, Date dateSortie, List<Genre> genres) {
        this.externalID = externalID;
        this.titre = titre;
        this.description = description;
        this.dateSortie = dateSortie;
        this.genres = genres;
    }
}
