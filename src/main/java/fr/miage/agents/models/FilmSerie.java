package fr.miage.agents.models;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilmSerie extends Oeuvre {

    private Duration duree;

    @OneToOne
    private Organisation producteur;

    @ManyToMany
    private List<Personne> acteurs = new ArrayList<Personne>();

    @ManyToMany
    private List<Personne> realisateurs = new ArrayList<Personne>();

    @ManyToMany
    private List<Personne> scenaristes = new ArrayList<Personne>();

    public FilmSerie(String externalID, String titre, String description, Date dateSortie, List<Genre> genres,
            Duration duree) {
        super(externalID, titre, description, dateSortie, genres);
        this.duree = duree;
    }
}
