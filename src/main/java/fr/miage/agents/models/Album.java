package fr.miage.agents.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Album extends Oeuvre {

    private String groupe;

    @OneToMany(mappedBy = "album")
    private List<Musique> musiques;
}
